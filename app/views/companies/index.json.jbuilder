json.array!(@companies) do |company|
  json.extract! company, :id, :name, :adresse, :city, :cp, :admin_id
  json.url company_url(company, format: :json)
end
