module NewsHelper

	def form_new

		simple_form_for(@new) do |f|
			f.input :email, label: 'votre email'
			f.input :submit
		end

	end
end
