class WebservicesController < ApplicationController

  def index
    @companies = Company.all
    respond_to do |format|
      format.json {render json: @companies}
    end
  end

  def company_nights
     @company = Company.where(name_company: params[:name]).all
     render json: @company

  end
  def title_companies
    @companiess = Company.select(:name_company,:id).distinct.all
    render json: @companiess
  end

  # affiche toutes les soirrées d'une company
  def night
    @night = Evening.where(company_id: params[:id])
    render json: @night
  end

end
