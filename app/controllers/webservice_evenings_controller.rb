class WebserviceEveningsController < ApplicationController
  def index
    @company = Company.all
    # @evenings = @company.evenings

    render json: @company

  end

  def show
    @evenings = @company.evenings

    respond_to do |format|
      format.json {render json: @evenings}
    end
  end
end
