class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  skip_before_filter  :protect_from_forgery
  # with: :exception


  rescue_from CanCan::AccessDenied do |exception|
    Rails.logger.debug "Access denied on #{exception.action} #{exception.subject.inspect}"

  end
  def current_ability
    @current_ability ||= Ability.new(current_admin)
  end

end
