class EveningsController < ApplicationController

  before_filter :set_resources

  def index

  end
  def new
    @evening = Evening.new
  end

  def create
    @evening = Evening.new(evening_param)
    @evening.company_id = @company.id
    if @evening.save
      redirect_to  @company, flash:{ success: "La soirée #{@evening.title} à bien été crée"}
    else
      flash.now[:error]= "Certain champs n'ont pas été corectement remplir"
      render "new"

    end
  end
  def edit
  end
  def update
    if @evening.update(evening_param)
      redirect_to  @company, flash:{ success: "La soirée #{@evening.title} à bien été crée"}
    else
      flash.now[:error]= "Certain champs n'ont pas été corectement remplir"
      render "edit"
    end
  end

  def destroy
    @evening.destroy
    redirect_to @company, flash:{ success: "Soirée supprimer"}
  end

  private
  def set_resources
    @company = Company.find(params[:company_id])
    authorize! :destroy, @company
    @evening = Evening.find(params[:id]) if params[:id]
  end
  def evening_param
      params.require(:evening).permit(:title, :nb_get_in, :description, :price, :company_id, :date_start, :date_end, :picture)
  end
end
