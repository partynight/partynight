class CompaniesController < ApplicationController

  before_filter :set_company, except: [:index, :new, :create, :nightIndex]
  #only: [:show, :edit, :update, :destroy]
  authorize_resource

  def index
    @companies = Company.where(admin_id: current_admin)
    @companie_name = Company.select(:name_company).where(admin_id: current_admin).distinct

    
    # @articles = Evening.search('Hip-Hop').records

  end

  def show
    @evenings = @company.evenings
  end

  def new
    @company = Company.new
  end


  def edit
  end


  def create
    @company = Company.new(company_params)

    @company.admin_id = current_admin.id
    respond_to do |format|
      if @company.save
        format.html { redirect_to @company, notice: 'Company was successfully created.' }
        format.json { render action: 'show', status: :created, location: @company }
      else
        format.html { render action: 'new' }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    respond_to do |format|
      if @company.update(company_params)
        format.html { redirect_to @company, notice: 'Company was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @company.destroy
    respond_to do |format|
      format.html { redirect_to companies_url }
      format.json { head :no_content }
    end
  end

  private

    def set_company
      @company = Company.find(params[:id])
    end


    def company_params
      params.require(:company).permit(:name, :adresse, :city, :cp, :admin_id, :name_company)
    end

end
