class Evening < ActiveRecord::Base

  # include Elasticsearch::Model
  # include Elasticsearch::Model::Callbacks

  #upload de files
  has_attached_file :picture,
    :styles => {
      :medium => "429×500>"
    },
    :default_url => "/images/:style/missing.png",
    :url => "/picture/:class/:attachment/:id/:style/:basename.:extension",
    :path => ":rails_root/public/picture/:class/:attachment/:id/:style/:basename.:extension"


  belongs_to :company

  validates :title, presence: true
  validates :description, presence: true
  validates :date_start, presence: true
  validates :date_end, presence: true
  # paperclip
  validates_attachment_content_type :picture, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]


  # Evening.search({
  #   filter:{
  #     terms:{'propertie.title' => ["test","test1"]}
  #   }
  # })
end
