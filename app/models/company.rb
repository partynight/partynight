class Company < ActiveRecord::Base

  has_many :evenings

  def all_evenings(id)
    @evenings = Evening.where(company_id: id)
    @evenings.to_json
  end

end
