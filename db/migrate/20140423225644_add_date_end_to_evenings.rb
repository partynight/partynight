class AddDateEndToEvenings < ActiveRecord::Migration
  def change
    add_column :evenings, :date_end, :datetime
  end
end
