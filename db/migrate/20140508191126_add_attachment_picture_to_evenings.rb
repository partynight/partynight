class AddAttachmentPictureToEvenings < ActiveRecord::Migration
  def self.up
    change_table :evenings do |t|
      t.attachment :picture
    end
  end

  def self.down
    drop_attached_file :evenings, :picture
  end
end
