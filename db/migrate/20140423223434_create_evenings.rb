class CreateEvenings < ActiveRecord::Migration
  def change
    create_table :evenings do |t|
      t.string :title
      t.integer :nb_get_in
      t.text :description
      t.integer :price
      t.integer :company_id

      t.timestamps
    end
  end
end
