# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Partynight::Application.config.secret_key_base = 'edf327ff766fddb12b75b584186c002f29b718aff6c87375339f8fe183da66169296faed8ab66a24cc52df4e44754fee6d37ca90026449f802eed194aeabe6d3'
